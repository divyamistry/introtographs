Introduction to Biological Networks and their Building Blocks
======

  * [*BioBook*](BioBook.md) contains the introduction relevant to biological networks
  * [*GenBook*](GenBook.md) contains almost the same content, but geared towards non-biological audience


Some informal introduction, and some formal formulation of what graphs/networks are. Examples and ideas are geared towards biological network application. 

The goal of this guide is to give the reader just enough information to be able to converse with others about their problems and common terms they may encounter. It should also be enough to get the reader started on looking at code and analysis related to biological networks. This guide is **not** meant to make the reader a professional in graph theory. For a complete formal introduction and useful discussions of network analysis and related topics, take a look at the following books.

  * [*Networks, Crowds, and Markets*](http://www.amazon.com/Networks-Crowds-Markets-Reasoning-Connected/dp/0521195330?tag=maofmi0e-20)
  * [*Introductory Graph Theory*](http://www.amazon.com/Introductory-Graph-Theory-Dover-Mathematics/dp/0486247759/ref=pd_sim_b_23?tag=maofmi0e-20)
  * [*Weighted Network Analysis: Applications in Genomics and Systems Biology*](http://www.amazon.com/Weighted-Network-Analysis-Applications-Genomics/dp/1441988181/ref=sr_1_8?tag=maofmi0e-20)
  * [*Statistical Analysis of Network Data: Methods and Models*](http://www.amazon.com/Statistical-Analysis-Network-Data-Statistics/dp/038788145X/ref=sr_1_9?tag=maofmi0e-20)
  * [*Statistical Analysis of Network Data with R*](http://www.amazon.com/Statistical-Analysis-Network-Data-Use/dp/1493909827/ref=sr_1_12?tag=maofmi0e-20) (I have not had a chance to read this book, but judging from the table of content, this has potential to be a very useful reference.)
