# Introduction to Graphs and its Building Blocks

## Informal introduction
A graph, also called a network, is a way to represent ideas, concepts, data, information, etc. in a way that shows the relationships between their units. An example graph/network is shown in figure below. 

![Brainstorming](images/brainstorm.jpg)

Brainstorming charts, mind maps, social networks, computer networks, and circuit diagrams are all examples of graphs. 

In many real-world scenarios, graphs are the most natural choice for thinking about the problem. If we were to understand friendships between people, a facebook-like social network might be a good way to go about it. In such a network, the individuals are connected to their friends. Imagine representing all the individuals by a small circle, and the individuals that are friends of each other, can be connected by a line. This creates a visual representation of a basic social network.

![Social Network Example](images/socialnetexample.jpeg)

In formal mathematical terms of graph/network, the circles representing the individuals are called nodes or vertices, and the lines connecting the individuals are called edges, links, or arcs. There is an edge between nodes representing Tom and Tristan, which means that Tom and Tristan are friends. Similarly, Tanya and Jessica are friends; however, they are not friends with any of the other individuals of this social group sample.

Let's try to look at the idea of graphs and networks in little more formal sense. That'll help understand how to store, process, and analyse graphs.

## Building blocks of a simple graph

### Defining a graph
A _simple_ graph is an ordered couple (2-tuple) of sets. The first of them being set of nodes, and the other being set of edges. For a graph _G_ with node set _V_ and edge set _E_, one would indicate a simple graph as _G = (V,E)_.

A simple graph may be represented as an adjacency matrix. An adjacency matrix _A_, is a square matrix with rows and columns representing the nodes. Each of the element of this matrix represents the edge value between corresponding row and column nodes.

### Nodes

Nodes are generally the elemental/unit pieces of data between which we want to establish some type of relationship. In a social network, individuals are the nodes of the network. In a protein-protein interaction network, the proteins are the nodes of the network.

### Edges

Edges are used to indicate some type of relationship between nodes. In a social network, an edge represents friendship between the individuals. In a protein-protein interaction network, an edge represents the interactivity between two proteins.

Edges commonly have two additional properties associated with them: direction and weight

##### Edge Direction
Edges with explicitly defined direction are called _directed edges_. Their use can be easily illustrated from a social network for a company. Let's say we want to understand the management hierarchy of a company. In such a network, all the nodes represent all the managers of a company. If a person _X_ manages a manager _Y_, a directed edge (shown here by an arrow) connects the tail of arrow at _X_ with the head of the arrow at _Y_.

![Management Social Network](images/officesocnetexample.jpeg) 

Based on this office social network, we can umambiguously say that (i) Buddy is everyone's boss, (ii) Jessica manages Tom and Kelly, (iii) Tanya, Ken, Carry, Tom, and Kelly do not manage any other managers.

A graph with directed edges is called a _directed graph_, or _digraph_ for short. A graph with undirected edges is, by extension, called an _undirected graph_.

##### Edge Weight
Perhaps for the same company, we want to see how many projects are shared by various employees. One way to model this scenario is designating a node for each employee, draw an undirected edge between employees that work on same projects, and assigning a value to that edge corresponding to the number of shared projects. This value is called an _edge weight_. If Tristan and Tom work on three projects, we'd draw an edge between them and assign value of 3 to the edge.

![Shared Projects Social Network](images/sharedprojsocnetexample.jpeg)

A graph with weighted edges is called a _weighted graph_. By extension, a graph without explicitly defined weights is called an _unweighted graph_.

### Subgraphs / Subnetworks
(remember to mention pathways here.)

## Types of networks

### Undirected

### Directed

### Weighted / Unweighted

### Cyclic / Non-cyclinc

### Complex graphs (multigraph, and hypergraph)

## Network Analysis

### Nodes

#### Node priority

#### Node perterbation

### Edges

#### Edge priority

#### Edge 

### Paths

#### Path Length

#### Path Search

#### Path Matching

# End
