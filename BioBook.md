# Introduction to Biological Networks and their Building Blocks

## Informal introduction
A graph, also called a network, is a way to represent ideas, concepts, data, information, etc. in a way that shows the relationships between their units. An example graph/network is shown in figure below. 

![Brainstorming](images/brainstorm.jpg)

Brainstorming charts, mind maps, social networks, computer networks, and circuit diagrams are all examples of graphs. 

In many real-world scenarios, graphs are the most natural choice for thinking about the problem. If we were to understand which proteins interact with each other in a cell, a protein-protein interaction network may be a good way to go aobut it. In such a network, the proteins are connected to their interaction partners. To summarise our data, maybe we can represent all individual proteins by a small circle, and the proteins that interact with each other can be connected by a line. This creates a visual representation of a protein-protein interaction network.

![Protein-Protein Interaction Network Example](images/ppinetexample.jpeg)

In the formal terms of graph/network, the circles representing the proteins are called nodes or vertices, and the lines connecting the proteins are called edges, links, or arcs. There is an edge between protein nodes **T** and **O**, which means that protein **T** and protein **O** interact with each other. Similarly, proteins **J** and **A** interact with each other; however, they do not have any other interacting partners in this sample of proteins on which some experiment was done.

Let's try to look at the idea of graphs and networks in little more formal mathematical sense. That'll help understand how to store, process, and analyse these networks.

## Building blocks of a simple graph

### Defining a graph
A _simple_ graph is an ordered couple (2-tuple) of sets. The first of them being set of nodes, and the other being set of edges. For a graph _G_ with node set _V_ and edge set _E_, one would indicate a simple graph as _G = (V,E)_.

A simple graph may be represented as an adjacency matrix. An adjacency matrix _A_, is a square matrix with rows and columns representing the nodes. Each of the element of this matrix represents the edge value between corresponding row and column nodes.

### Nodes

Nodes are generally the elemental/unit pieces of data between which we want to establish some type of relationship. In a protein-protein interaction network, the proteins are the nodes of the network. In a gene coexpression network, the genes are the nodes of the network.

### Edges

Edges are used to indicate some type of relationship between nodes. In a protein-protein interaction network, an edge can represent the interactivity between two proteins. In a gene coexpression network, an edge can represent the coexpression between the genes it connects.

Edges commonly have two additional properties associated with them: direction and weight

##### Edge Direction
Edges with explicitly defined direction are called _directed edges_. Their use can be easily illustrated using a regulatory network. Let's say we want to understand which genes regulate which other genes. In such a network, all the nodes represent the genes of interest. If a gene **gX** up-regulates gene **gY**, a directed edge (shown here by an arrow) connects the tail of arrow at **gX** with the head of the arrow at **gY**.

![Management Social Network](images/ppidigraphexample.jpeg) 

Based on this example network, we can umambiguously say that (i) **gB** is the root of this regulation network, (ii) **gJ** up-regulates both genes **gE** and **gO**, (iii) genes **gE**, **gO**, **gC**, **gA**, and **gK** do not up-regulate any of the other genes of interest.

A graph with directed edges is called a _directed network_ or _directed graph_; _digraph_ for short. A graph with undirected edges is, by extension, called an _undirected graph_ or _undirected network_.

##### Edge Weight
Perhaps for the same PPI network seen earlier, we want to see how many residues are shared by various proteins. One way to model this scenario is designating a node for each protein, drawing an undirected edge between proteins that interact with each other, and assigning a value to that edge corresponding to the number of shared residues. This value is called an _edge weight_. If proteins **T** and **O** share 30 residues, we'd draw an edge between them and assign value of 30 to the edge.

![Shared Projects Social Network](images/sharedresiduenetexample.jpeg)

A graph with weighted edges is called a _weighted graph_. By extension, a graph without explicitly defined weights is called an _unweighted graph_.

### Subgraphs / Subnetworks
<!-- (remember to mention pathways here.) -->

## Types of networks

### Undirected

### Directed

### Weighted / Unweighted

### Cyclic / Non-cyclinc

### Complex graphs (multigraph, and hypergraph)

## Network Analysis

In a biological network, interpretations of cycles, paths, subnetworks, centralities, etc. phenomena are not very clear. More over, the interpretation can change based on the statistical underpinnings of how the edges were created.

Following notes are to help you start thinking about some of the possibilities of biological interpretations of these various network features.

### Nodes

#### Node priority

#### Node perterbation

### Edges

#### Edge priority

#### Edge 

### Paths

#### Path Length

#### Path Search

#### Path Matching

# End
